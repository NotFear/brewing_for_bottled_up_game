// ALGO_TEST_SHENANIGANS
// This script is made with the primary goal of...
// 1. Testing how duration would end up affecting base_effect_power
// 2. Penalties that arise out of different durations
// 3. Testing out the potency multiplier

// What will NOT be included in this test is:
// 1. Direct tier effects on base_effect_power
// 2. Outside effects that alter any of the stats of the potions

// This script thus serves as a way to test penalties that arise out of duration and base_effect_power and how it would interact with potency.
// If curious, in the actual potion maker the base_effect_power will be directly modified by the base_duration penalties BEFORE tier. Should be by mathematical rule commutative so order should not matter.

// Import the std::fs module for file I/O
use std::fs;

struct StatusEffect {
    effect_name:String,
    base_effect_power:f32,
    base_duration:f32,
    tier:usize,
}

struct Ingredient<'a> {
    name:String,
    status_effect: &'a StatusEffect,
    amount:u32,
}

struct TierWeight {
    weights:[f32; 6],
}


// Note, it apparently would be preferable to have a unified duration for the potion?

fn main() {

    // What we will input into a function. No need for mutability
    let stat_eff_arr:Vec<StatusEffect> = Vec::from([
        StatusEffect {effect_name:String::from("Strength"),       base_effect_power:2.0,      base_duration: 300.0,    tier:  1},
        StatusEffect {effect_name:String::from("Regeneration"),   base_effect_power:10.0,     base_duration: 10.0,     tier:  4},
        StatusEffect {effect_name:String::from("Speed"),          base_effect_power:20.0,     base_duration: 200.0,    tier:  2},
        StatusEffect {effect_name:String::from("FireResistance"), base_effect_power:20.0,     base_duration: 60.0,     tier:  1},
        StatusEffect {effect_name:String::from("CloudWalk"),      base_effect_power:10.0,     base_duration: 60.0,     tier:  1},
        StatusEffect {effect_name:String::from("WaterBreathing"), base_effect_power:15.0,     base_duration: 120.0,    tier:  3},
        StatusEffect {effect_name:String::from("Thorns"),         base_effect_power:100.0,    base_duration: 45.0,     tier:  1},
        StatusEffect {effect_name:String::from("Jump"),           base_effect_power:25.0,     base_duration: 200.0,    tier:  3},
        StatusEffect {effect_name:String::from("MagicPower"),     base_effect_power:50.0,     base_duration: 45.0,     tier:  5},
        StatusEffect {effect_name:String::from("NightVision"),    base_effect_power:10.0,     base_duration: 150.0,    tier:  1},
    ]);


    // This is what we will input into the functions.
    let mut ingredient_arr:Vec<Ingredient> = Vec::from([
        Ingredient {name: String::from("ing_1"), status_effect: &stat_eff_arr[0], amount: 1},
        Ingredient {name: String::from("ing_2"), status_effect: &stat_eff_arr[1], amount: 120},


        //Ingredient {name: String::from("ing_3"), status_effect: &stat_eff_arr[2], amount: 1},
        //Ingredient {name: String::from("ing_4"), status_effect: &stat_eff_arr[3], amount: 1},
        //Ingredient {name: String::from("ing_5"), status_effect: &stat_eff_arr[4], amount: 1},
        //Ingredient {name: String::from("ing_6"), status_effect: &stat_eff_arr[5], amount: 1},
        //Ingredient {name: String::from("ing_7"), status_effect: &stat_eff_arr[6], amount: 1},
        //Ingredient {name: String::from("ing_8"), status_effect: &stat_eff_arr[7], amount: 1},
    ]);

    // Defining weights here.
    let defined_weights:TierWeight = TierWeight { weights: [1.0, 1.0, 2.0, 4.0, 8.0, 16.0] };

    // We first use method one to get the duration.
    let method_1_calculated_duration:f32 = method_one_duration(&mut ingredient_arr, defined_weights);

    // Then we use the second method to get the duration.
    let method_2_calculated_duration:f32 = method_two_duration(&mut ingredient_arr);

    println!("Method 1 duration avg: {}", method_1_calculated_duration);
    println!("Method 2 duration avg: {}", method_2_calculated_duration);

    // Next up will involve attempting to find how the base effect power is affected by the difference in duration.
    // The penalties for both methods revealed!

    // Method 1 penalties...
    let ingredient_arr_ref:&mut[Ingredient] = &mut ingredient_arr;
    println!("\nMETHOD 1 EFFECT POWER: ");

    for this_ingredient in &mut *ingredient_arr_ref {
        let calculated_dur_base_effect:f32 = modify_base_effect_by_output_duration(this_ingredient, method_1_calculated_duration);
        let calculated_potency_modifier:f32 = potency_calculator(this_ingredient);
        let calculated_base_effect:f32 = calculated_dur_base_effect*(1.0 + calculated_potency_modifier);

        let mut effect_output:String = String::from(this_ingredient.status_effect.effect_name.clone());


        effect_output.push_str(" power change: ");
        effect_output.push_str(this_ingredient.status_effect.base_effect_power.to_string().as_str());
        effect_output.push_str(" -> ");
        effect_output.push_str(calculated_base_effect.to_string().as_str());
        println!("{}", effect_output)
    }

    // Method 2 penalties...
    println!("\nMETHOD 2 EFFECT POWER: ");

    for this_ingredient in &mut *ingredient_arr_ref {
        let calculated_dur_base_effect:f32 = modify_base_effect_by_output_duration(this_ingredient, method_2_calculated_duration);
        let calculated_potency_modifier:f32 = potency_calculator(this_ingredient);
        let calculated_base_effect:f32 = calculated_dur_base_effect*(1.0 + calculated_potency_modifier);

        let mut effect_output:String = String::from(this_ingredient.status_effect.effect_name.clone());
        effect_output.push_str(" power change: ");
        effect_output.push_str(this_ingredient.status_effect.base_effect_power.to_string().as_str());
        effect_output.push_str(" -> ");
        effect_output.push_str(calculated_base_effect.to_string().as_str());
        println!("{}", effect_output)
    }

}

// Method 1: Using weights through tiers.
fn method_one_duration(this_ingredient_arr:&mut [Ingredient], this_weight:TierWeight) -> f32 {

    // We will need to do two sums, one for the value and weight and one for standalone weights.
    let mut sum_with_weight_and_value:f32 = 0.0;

    for this_ingredient in &mut *this_ingredient_arr {

        sum_with_weight_and_value = sum_with_weight_and_value + this_ingredient.status_effect.base_duration*this_weight.weights[this_ingredient.status_effect.tier];

    }

    // Now for the net part of the sum
    let mut sum_with_weight:f32 = 0.0;

    for this_ingredient in &mut *this_ingredient_arr {

        sum_with_weight = sum_with_weight + this_weight.weights[this_ingredient.status_effect.tier];

    }

    // Final calculation for output
    let calculated_duration:f32 = sum_with_weight_and_value/sum_with_weight;

    return calculated_duration;
}

// Method 2: Using the formula
// For this, we will need to consider taking an x length array, and slowly removing elements from it
fn method_two_duration(this_ingredient_arr:&mut [Ingredient]) -> f32 {
    let mut max_val:f32 = 0.0;
    let mut min_val:f32 = this_ingredient_arr[0].status_effect.base_duration;
    let mut max_index:usize = 0;
    let mut min_index:usize = 0;

    let arr_size:usize = this_ingredient_arr.len();

    let mut cond_arr:Vec<bool> = Vec::new(); // Creates a vec object to cond_arr
    cond_arr.resize(arr_size, true); // Resizes and fills with trues.
    let read_cond_arr = &mut cond_arr; // We make a separate array where we have references to the original cond_arr values. This can be read/inputted into.


    // Finds initial max/min val.
    for (i, this_ingredient) in this_ingredient_arr.iter().enumerate() {

        if this_ingredient.status_effect.base_duration > max_val {
            max_val = this_ingredient.status_effect.base_duration;
            max_index = i;
        }

        if this_ingredient.status_effect.base_duration < min_val {
            min_val = this_ingredient.status_effect.base_duration;
            min_index = i;
        }
    }

    // Two of the conditional arrays will be set to false.
    read_cond_arr[max_index] = false;
    read_cond_arr[min_index] = false;

    // And we get our current average calculation.
    let mut current_average:f32 = f32::sqrt(max_val/min_val) * min_val;

    // If we encounter a scenario where we have more than two elements, then we will use an alternate form of the formula.
    // Otherwise, the initial average calculation will do.
    if arr_size > 2 {
        // Now we simply retry this while loop until every element in the cond array is set to false...
        let mut is_arr_all_false:bool = is_this_arr_all_false(read_cond_arr);

        // A bug that occurs is when we only have two elements here.
        while is_arr_all_false == false {

            // Multiply prev average with previous min val...
            min_val = max_val;

            for (i, this_element) in (&mut *read_cond_arr).iter().enumerate() { // This essentially allows the re-use of a reference. Apparently you "consume" a reference if you use it once, so you have to de-ref and then re-ref.
                if this_element == &true {
                    if this_ingredient_arr[i].status_effect.base_duration < min_val {
                        min_val = this_ingredient_arr[i].status_effect.base_duration ;
                        min_index = i;
                    }
                }
            }

            // What remains after must be the next min val without a doubt.
            // We get the next current avg equation based on which is larger and which is not larger.
            if current_average > min_val {
                current_average = f32::sqrt(current_average/min_val) * min_val
            }
            else if current_average < min_val {
                current_average = f32::sqrt(min_val/current_average) * current_average
            }

            read_cond_arr[min_index] = false;

            is_arr_all_false = is_this_arr_all_false(read_cond_arr);
        }
    }

    return current_average;
}

// Would extend an implementation to the Vec template,
fn is_this_arr_all_false(this_arr:&Vec<bool>) -> bool {
    for this_element in this_arr {
        if this_element == &true {
            return true;
        }
    }
    return false;
}


// This function will modify the base effect power of a potion if it is set to be modifiable by the duration.
// We want something like... 100% increase in duration leads to 25% decrease in power or similar.
// The modifier effect can go both ways: be a buff or a nerf.
fn modify_base_effect_by_output_duration(this_ingredient:&Ingredient, this_duration:f32) -> f32 {

    // If you want to change how impactful the penalty is, use these constants...
    const POWER_MODIFIER:f32 = 1.0;
    const LINEAR_MODIFIER:f32 = 0.50;
    const DURATION_DIFFERENCE_TILL_PENALTY:f32 = 0.5; // A 50% duration difference until penalty.

    let mut duration_difference:f32 = this_ingredient.status_effect.base_duration - this_duration; // Initially used to check if negative/positive, then used to see how much duration % difference there is.

    let mut is_penalty:bool = false;

    let mut modifier_effect:f32 = 0.0;
    let mut adjusted_base_effect:f32 = 0.0; // Output modifier.

    // Differentiate if the duration difference is positive or negative.
    if duration_difference < 0.0 {
        duration_difference = this_duration/this_ingredient.status_effect.base_duration;
        is_penalty = false;
    }
    else if duration_difference > 0.0 {
        duration_difference = this_ingredient.status_effect.base_duration/this_duration; // b
        is_penalty = true;
    }

    // Should we apply a modifier to the base_effect_power?
    if duration_difference > (1.0 + DURATION_DIFFERENCE_TILL_PENALTY) {

        // Where we calculate the modifier_effect.
        let duration_difference_offset = 1.0 + DURATION_DIFFERENCE_TILL_PENALTY;
        modifier_effect = 1.0 + LINEAR_MODIFIER*f32::powf(duration_difference - duration_difference_offset, POWER_MODIFIER);

        // If it is penalty, we apply a debuff. If not, we do not!
        if is_penalty == true {
            adjusted_base_effect = this_ingredient.status_effect.base_effect_power*modifier_effect; // or this_ingredient_arr.base_effect_power*(1.0 / modifier_effect)
        }
        else if is_penalty == false {
            adjusted_base_effect = this_ingredient.status_effect.base_effect_power/modifier_effect;
        }
    }
    else {
        // If DURATION_DIFFERENCE_TILL_PENALTY is not hit, then we simply return our normal base effect power.
        adjusted_base_effect = this_ingredient.status_effect.base_effect_power;
    }

    return adjusted_base_effect;
}


// If we add additional ingredient, we want to increase the potency of the status effect associated with said inventory.
// This potency limit is set to be a maximum of 50% of its base effect power plus tier (Tier is not done in this script for increasing base power though!).
fn potency_calculator(this_ingredient:&Ingredient) -> f32 {

    let item_amount:f32 = this_ingredient.amount as f32;
    const DOUBLE_RATE:f32 = 20.0; // For every 20 material, double!
    const POTENCY_MOD:f32 = 0.1; // So with 0.1, goes in steps of 10% up to a maximum of 50%.

    // In order to see our next potency level, we calculate how much of it has been added using this equation...
    // log_2(M_r/D_r) + 1 = S
    // This penalty is applied for every D_r (Double rate) of each material added M_r, and this penalty is a cumulative 2x increase in material with default D_r of 20.

    // The potency calculator will go through 5 stages, each with 10% changes in potency.
    // So the expected maximum potency penalty is ~93%, where a material will require a whooping 1600% more resources for the next potency level.
    // Things to think about, how will potency work with higher tiered materials of the same status effect?
    // Probably have a penalty on top of a penalty (EG Reduce material contribution if tier 2 by x percent, probably 50%).
    let mut stage:f32 = 0.0;

    if item_amount >= DOUBLE_RATE {
        stage = (item_amount/DOUBLE_RATE).log2().floor() + 1.0;
    }

    let potency_modifier:f32 = POTENCY_MOD*stage;

    println!("The potency modifier ended up being: {}", potency_modifier);

    return potency_modifier;

}

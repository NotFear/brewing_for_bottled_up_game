# Project: Brewing Prototype
This is some Rust code that aims to test out the brewing system that will be implemented into Bottled Up.
If you wish to know more about the theory that went behind this prototype, have a read through this blog post:

https://notfear.gitlab.io/projects/project_brewing_bu/

To build the binaries needed for your system, simply run the following command:

` cargo build --release `

Then find the program under...

`target/release/grrs`

I intend to extend this script with additional json functionality in the future for. There will be an update for that, but at this point this project is complete.

# License
MIT
